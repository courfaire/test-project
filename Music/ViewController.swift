//
//  ViewController.swift
//  Music
//
//  Created by Paul Breaks on 18.08.2021.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var player: AVAudioPlayer?
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func playSound() {
            guard let url = Bundle.main.url(forResource: "ChillingMusic", withExtension: "wav") else { return }
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                try AVAudioSession.sharedInstance().setActive(true)
                
                player = try AVAudioPlayer(contentsOf: url)
                if let player = player {
                    player.play()
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }

    @IBAction func playButtonPressed(_ sender: UIButton) {
        
        playSound()
        
    }
    
}

